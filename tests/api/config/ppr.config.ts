global.appConfig = {
    envName: "PPR Environment",
    baseUrl: "http://tasque.lol/api/",
    swaggerUrl: "http://tasque.lol/index.html",

    user: {
        avatar: "http://site.com/image.jpg",
        email: "ksenia321@gmail.com",
        userName: "Ksenia",
        password: "qwe12345",
    },
    userToUpdate: {
        avatar: "http://site.com/image2.jpg",
        email: "ksenia321@gmail.com",
        userName: "Ksenechka",
    },
    newPost: {
        previewImage: "http://site.com/image.jpg",
        body: "Text of my post",
    },
    comment: "My super comment!",
};
