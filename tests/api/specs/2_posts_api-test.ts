import { expect } from "chai";
import chai from "chai";
import chaiJson from "chai-json-schema";
import { AuthController, LoginSchema, RegistrationSchema } from "../lib/controllers/auth.controller";
import {
    checkResponseJSONSchema,
    checkResponseTime,
    checkStatusCode,
} from "../../helpers/functionsForChecking.helper";
import {
    PostsController,
    NewPostSchema,
    LikePostSchema,
    CommentPostSchema,
} from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";

chai.use(chaiJson);

const registerSchema = require("./schemas/register_schema.json");
const postsSchema = require("./schemas/posts_schema.json");
const postSchema = require("./schemas/post_schema.json");
const commentSchema = require("./schemas/comment_schema.json");

describe("POSTS", () => {
    let accessToken: string;
    let userId: number;
    let postId: number;

    const auth = new AuthController();
    const posts = new PostsController();
    const users = new UsersController();

    before(`Register user before tests`, async () => {
        let body: RegistrationSchema = global.appConfig.user;
        let response = await auth.registration(body);

        checkStatusCode(response, 201);
        checkResponseTime(response);
        checkResponseJSONSchema(response, registerSchema);
    });

    it(`Login user`, async () => {
        let body: LoginSchema = {
            email: global.appConfig.user.email,
            password: global.appConfig.user.password,
        };
        let response = await auth.login(body);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, registerSchema);
        expect(response.body.user.userName, `Check user name`).to.be.equal(global.appConfig.user.userName);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
    });

    it(`Get all posts`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, postsSchema);
    });

    it(`Create new post`, async () => {
        let body: NewPostSchema = {
            authorId: userId,
            ...global.appConfig.newPost,
        };
        let response = await posts.newPost(body, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, postSchema);

        postId = response.body.id;
    });

    it(`Like the post`, async () => {
        let body: LikePostSchema = {
            entityId: postId,
            isLike: true,
            userId: userId,
        };
        let response = await posts.likePost(body, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
    });

    it(`Add comment to the post`, async () => {
        let body: CommentPostSchema = {
            authorId: userId,
            postId: postId,
            body: global.appConfig.comment,
        };
        let response = await posts.addPostComment(body, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, commentSchema);
    });

    it(`Check new post`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response);

        const currentPost = response.body.filter((post) => post.id === postId)[0];
        const comment = currentPost.comments[0];
        const reaction = currentPost.reactions[0];

        checkResponseJSONSchema(
            {
                body: currentPost,
            },
            postSchema
        );
        expect(currentPost.id, `Check created post`).to.be.equal(postId);
        expect(comment.author.id, `Check comment author`).to.be.equal(userId);
        expect(comment.body, `Check post comment`).to.be.equal(global.appConfig.comment);
        expect(reaction.user.id, `Check reaction author`).to.be.equal(userId);
        expect(reaction.isLike, `Check post reaction`).to.be.equal(true);
    });

    after(`Delete user after tests`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response);
    });
});
