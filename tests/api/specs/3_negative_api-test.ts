import { expect } from "chai";
import { AuthController, LoginSchema, RegistrationSchema } from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { PostsController, NewPostSchema, LikePostSchema } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";

describe("NEGATIVE", () => {
    let accessToken: string;
    let userId: number;

    const auth = new AuthController();
    const posts = new PostsController();
    const users = new UsersController();

    before(`Register and get the token and id`, async () => {
        let body: RegistrationSchema = global.appConfig.user;
        let response = await auth.registration(body);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
    });

    it(`Login with fake credential`, async () => {
        let body: LoginSchema = {
            email: "fake@user.com",
            password: "fake-password",
        };

        let response = await auth.login(body);

        checkStatusCode(response, 404);
        checkResponseTime(response);
        expect(response.body.error, `Check error message`).to.be.equal("Entity User was not found.");
    });

    it(`Create post without token`, async () => {
        let body: NewPostSchema = {
            authorId: userId,
            ...global.appConfig.newPost,
        };
        let response = await posts.newPost(body, "");

        checkStatusCode(response, 401);
        checkResponseTime(response);
        expect(response.statusMessage, `Check error message`).to.be.equal("Unauthorized");
    });

    it(`Add like to not created post`, async () => {
        let body: LikePostSchema = {
            entityId: 99999,
            isLike: true,
            userId: userId,
        };
        let response = await posts.likePost(body, accessToken);

        checkStatusCode(response, 500);
        checkResponseTime(response);
        expect(response.body.error, `Check error message`).to.be.equal(
            "An error occurred while saving the entity changes. See the inner exception for details."
        );
    });

    after(`Delete user after tests`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response);
    });
});
