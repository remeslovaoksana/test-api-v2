import { expect } from "chai";
import chai from "chai";
import chaiJson from "chai-json-schema";
import { AuthController, RegistrationSchema, LoginSchema } from "../lib/controllers/auth.controller";
import {
    checkResponseJSONSchema,
    checkResponseTime,
    checkStatusCode,
} from "../../helpers/functionsForChecking.helper";
import { UsersController, UpdateUserSchema } from "../lib/controllers/users.controller";

const userSchema = require("./schemas/user_schema.json");
const usersSchema = require("./schemas/users_schema.json");
const registerSchema = require("./schemas/register_schema.json");

chai.use(chaiJson);

describe("USER", () => {
    let accessToken: string;
    let userId: number;

    const auth = new AuthController();
    const users = new UsersController();

    it(`Register user`, async () => {
        let body: RegistrationSchema = global.appConfig.user;
        let response = await auth.registration(body);

        checkStatusCode(response, 201);
        checkResponseTime(response);
        checkResponseJSONSchema(response, registerSchema);
        expect(response.body.user.avatar, `Check user avatar`).to.be.equal(global.appConfig.user.avatar);
        expect(response.body.user.email, `Check user email`).to.be.equal(global.appConfig.user.email);
        expect(response.body.user.userName, `Check user name`).to.be.equal(global.appConfig.user.userName);
    });

    it(`Get all users`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, usersSchema);
    });

    it(`Login user`, async () => {
        let body: LoginSchema = {
            email: global.appConfig.user.email,
            password: global.appConfig.user.password,
        };
        let response = await auth.login(body);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, registerSchema);
        expect(response.body.user.userName, `Check user name`).to.be.equal(global.appConfig.user.userName);

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
    });

    it(`Get user from token`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, userSchema);
        expect(response.body.id, `Check user id`).to.be.equal(userId);
    });

    it(`Update user`, async () => {
        let body: UpdateUserSchema = {
            id: userId,
            ...global.appConfig.userToUpdate,
        };
        let response = await users.updateUser(body, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response);
    });

    it(`Get updated user from token`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, userSchema);
        expect(response.body.id, `Check user id`).to.be.equal(userId);
        expect(response.body.userName, `Check user name`).to.be.equal(global.appConfig.userToUpdate.userName);
    });

    it(`Get user by id`, async () => {
        let response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response);
        checkResponseJSONSchema(response, userSchema);
        expect(response.body.id, `Check user id`).to.be.equal(userId);
        expect(response.body.avatar, `Check user avatar`).to.be.equal(global.appConfig.userToUpdate.avatar);
        expect(response.body.email, `Check user email`).to.be.equal(global.appConfig.userToUpdate.email);
        expect(response.body.userName, `Check user name`).to.be.equal(global.appConfig.userToUpdate.userName);
    });

    it(`Delete user by id`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response);
    });
});
