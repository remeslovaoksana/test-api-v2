import {
    checkResponseTime,
    checkStatusCode,
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

describe ("TEST DATA SET FOR LOGIN", () => {
    let invalidCredentialsDataSet = [
        { email: 'ksenia321@mail.com', password: '1234' },
        { email: 'ksenia321@mail.com', password: '4321' },
        { email: 'ksenia321@mail.com', password: 'qzxcbgf' },
        { email: 'ksenia321@mail.com', password: 'qwe123456' },
        { email: 'ksenia321@mail.com', password: 'qwe12' },
        { email: '', password: 'qwe12d345' },
        { email: 'ksenia321@mail.com', password: '' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials);

            checkStatusCode(response, 401); 
            checkResponseTime(response);
        });
    });

})