export {};
declare global {
    namespace NodeJS {
        interface Global {
            appConfig: {
                envName: string;
                baseUrl: string;
                swaggerUrl: string;
                users: {
                    avatar: string;
                    email: string;
                    userName: string;
                    password: string;
                };
                userToUpdate: {
                    avatar: string;
                    email: string;
                    userName: string;
                };
                newPost: {
                    previewImage: string;
                    body: string;
                };
                comment: string;
            };
        }
    }
}
